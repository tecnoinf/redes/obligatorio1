# Obligatorio 1

## Como usar con Clion

###### 1- Clonar el repositorio: `git clone https://gitlab.com/tecnoinf/redes/obligatorio1.git`

###### 2- Ir a la pantalla de bienvenida de CLion y elegir la opción `Import Project from Sources`:

![Welcome CLion](http://i.imgur.com/yYqlZVv.png)

###### 3- Seleccionar el directorio del repositorio recien clonado (en este caso `obligatorio1`), y presionar `OK`:

![Directory selection](http://i.imgur.com/eHrtjoY.png)

###### 4- Seleccionar los archivos de codigo fuente que pertenecen al proyecto (`.cpp` y `.h`) y presionar `OK`

###### 5- Si los archivos correspondientes al proyecto de CLion no estan ignorados (en un archivo `.gitignore`), se debe crear un archivo `.gitignore` en la raiz del proyecto y agregar el siguiente contenido:
###### 6- En Ubuntu 16.04 fue necesario instalar libssl-dev
```
# Ignores para el IDE CLion
CMakeLists.txt
.idea/
cmake-build-debug/

```
