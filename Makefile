All: mods/autenticar.o mods/envioMensaje.o mods/Logger.o mods/mensenhales.o mods/recepcionMensaje.o mods/reloj.o mods_externos/md5.o
	if [ -d dist ]; then rm -Rf dist; fi
	mkdir dist
	g++ -std=c++14 -Wall -o dist/mensajeria mensajeria.cpp mods/autenticar.o mods/envioMensaje.o mods/Logger.o mods/mensenhales.o mods/recepcionMensaje.o mods/reloj.o mods_externos/md5.o

mods/autenticar.o: mods/autenticar.h mods/autenticar.cpp
	g++ -std=c++14 -Wall -c -o mods/autenticar.o mods/autenticar.cpp

mods/envioMensaje.o: mods/envioMensaje.h mods/envioMensaje.cpp
	g++ -std=c++14 -Wall -c -o mods/envioMensaje.o mods/envioMensaje.cpp

mods/Logger.o: mods/Logger.h mods/Logger.cpp
	g++ -std=c++14 -Wall -c -o mods/Logger.o mods/Logger.cpp

mods/mensenhales.o: mods/mensenhales.h mods/mensenhales.cpp
	g++ -std=c++14 -Wall -c -o mods/mensenhales.o mods/mensenhales.cpp

mods/recepcionMensaje.o: mods/recepcionMensaje.h mods/recepcionMensaje.cpp
	g++ -std=c++14 -Wall -c -o mods/recepcionMensaje.o mods/recepcionMensaje.cpp

mods/reloj.o: mods/reloj.h mods/reloj.cpp
	g++ -std=c++14 -Wall -c -o mods/reloj.o mods/reloj.cpp

mods_externos/md5.o: mods_externos/md5.h mods_externos/md5.cpp
	g++ -std=c++14 -Wall -c -o mods_externos/md5.o mods_externos/md5.cpp

clean:
	rm -f mods/*.o
	rm -f mods_externos/*.o
	rm -fR dist
