//
// Created by sebiitta on 04/06/18.
//

#include <netinet/in.h>
#include <unistd.h>
#include <cstdlib>
#include <list>
#include <cstdio>
#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <strings.h>
#include <arpa/inet.h>
#include <cstring>
#include <string>
#include <netdb.h>
#include <stdexcept>
#include "Logger.h"
#include "constantes.h"

using std::cout;
using std::endl;
using std::string;
using std::list;
using std::runtime_error;

extern list<int *> fileDescriptors;

void logger() {
    int fd, fd2, err;
    struct sockaddr_in server{}, client{};
    bool done = false;
    socklen_t sinSize;
    ssize_t bufLen;
    char buf[MAX_LARGO_MENSAJE];

    FILE *log;

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        throw runtime_error("[logger] Error socket()");
    }

    server.sin_family = AF_INET;
    server.sin_port = htons((uint16_t) LOGGER_PORT);
    server.sin_addr.s_addr = INADDR_ANY;

    bzero(&(server.sin_zero), 8);  // Escribimos ceros en el resto de la estructura

    err = bind(fd, (struct sockaddr*) &server, sizeof(struct sockaddr));
    if (err == -1) {
        throw runtime_error("[logger] Error en bind()");
    }

    fileDescriptors.push_back(&fd);

    err = listen(fd, CONNS_LIMIT);
    if (err == -1) {
        throw runtime_error("[logger] Error en listen()");
    }

    while (! done) {
        sinSize = sizeof(struct sockaddr_in);

        fd2 = accept(fd, (struct sockaddr*) &client, &sinSize);
        if (fd2 == -1) {
            throw runtime_error("[logger] Error en accept()");
        }

        // Proceso hijo.
        string msj = "Servidor de loggin.\r\n";

        send(fd2, msj.c_str(), msj.size(), 0);

        bufLen = recv(fd2, buf, MAX_LARGO_MENSAJE, 0);
        if (bufLen == -1) {
            throw runtime_error("[logger] Error en recv()");
        }
        buf[bufLen - 2] = '\0';

        log = fopen("salidas.log", "a+");
        fprintf(log, "%s\n", buf);
        fclose(log);


        if (strcmp(buf, "/salir") == 0) {
            done = true;
        }
        close(fd2);
    }

    close(fd);

    cout << "[" << getpid() << "] " << "Saliendo" << endl;
}

bool loggear(string mensaje) {
    int fd, err;
    struct sockaddr_in client{};
    ssize_t bufLen;
    char buf[MAX_LARGO_MENSAJE];
    struct hostent *he;

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        cout << "[loggear] Error al crear el socket" << endl;
        return true;
    }

    he=gethostbyname("127.0.0.1");
    if (he == nullptr) {
        cout << "[loggear] Error en gethostbyname()" << endl;
        return true;
    }

    client.sin_family = AF_INET;
    client.sin_port = htons((uint16_t) LOGGER_PORT);
    client.sin_addr = *((struct in_addr *) he->h_addr);

    bzero(&(client.sin_zero), 8);  // Escribimos ceros en el resto de la estructura

    err = connect(fd, (struct sockaddr*) &client, sizeof(struct sockaddr));
    if (err == -1) {
        cout << "[loggear] Error en conect()" << endl;
        return true;
    }

    bufLen = recv(fd, buf, MAX_LARGO_MENSAJE, 0);
    if (bufLen == -1) {
        cout << "[loggear] Error en recv()" << endl;
        return true;
    }
    buf[bufLen - 2] = '\0';

    if (strcmp(buf, "Servidor de loggin.") == 0) {
        mensaje += "\r\n";
        err = (int) send(fd, mensaje.c_str(), mensaje.size(), 0);
        if (err == -1) {
            cout << "[loggear] error en send()" << endl;
            return true;
        }
    }

    close(fd);

    return false;
}
