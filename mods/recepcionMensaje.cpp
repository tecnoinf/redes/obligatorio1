
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string>
#include <cstdio>
#include <netdb.h>
#include "constantes.h"
#include <cstring>
#include "constantes.h"
#include <iostream>
#include <list>
#include <sstream>
#include <stdexcept>
#include "recepcionMensaje.h"
#include "envioMensaje.h"
#include "reloj.h"
#include "Logger.h"

/* El puerto que será abierto */
#define BACKLOG 6 /* El número de conexiones permitidas */

using namespace std;

extern list<int *> fileDescriptors;

void recepcionMensaje(int puerto, string ipLocal)
{
    char buf[MAX_LARGO_MENSAJE];
    int fd, fd2, numbytes; /* los ficheros descriptores */
    struct sockaddr_in server{};  /* para la información de la dirección del servidor */
    struct sockaddr_in client{};  /* para la información de la dirección del cliente */
    socklen_t sin_size;      /* A continuación la llamada a socket() */

    if ((fd=socket(AF_INET, SOCK_STREAM, 0)) == -1 ) {
        throw runtime_error("[recepcionMensaje] Error en socket()");
    }

    server.sin_family = AF_INET;
    server.sin_port = htons((uint16_t) puerto);  /* ¿Recuerdas a htons() de la sección "Conversiones"? =) */
    server.sin_addr.s_addr = INADDR_ANY;    /* INADDR_ANY coloca nuestra dirección IP automáticamente */
    bzero(&(server.sin_zero),8);     /* escribimos ceros en el resto de la estructura */

    /* A continuación la llamada a bind() */
    if(bind(fd,(struct sockaddr*)&server, sizeof(struct sockaddr))==-1) {
        throw runtime_error("[recepcionMensaje] Error en bind()");
    }

    if(listen(fd, BACKLOG) == -1) {  /* llamada a listen() */
        throw runtime_error("[recepcionMensaje] Error en listend()");
    }

    fileDescriptors.push_back(&fd);

    while(1) {
        sin_size=sizeof(struct sockaddr_in);
        /* A continuación la llamada a accept() */

        if ((fd2 = accept(fd,(struct sockaddr *)&client, &sin_size))==-1) {
            throw runtime_error("[recepcionMensaje] Error en accept()");
        }

        if ((numbytes = (int) recv(fd2,buf,MAX_LARGO_MENSAJE,0)) == -1){ /* llamada a recv() */
            throw runtime_error("[recepcionMensaje] Error en recv()");
        }

        string clock = reloj();

        buf[numbytes-2] = '\0';

        string msjjj = (string) buf;

        if (msjjj.substr(0, 5) == "&file") {


            string aux = msjjj.substr(6);

            stringstream ss;
            ss << aux;
            string user, arch;
            getline(ss, user, '|');
            getline(ss, arch);


            sleep(1);
            receive_image(fd2, arch);

            string mens= clock + " "+ inet_ntoa(client.sin_addr) + " " + "archivo recibido: " + arch + " de: " + user;    /* Mostrará la IP del remitente, nombre de archivo recibido y usuario:  */
            loggear(mens);

            cout << "<Archivo recibido de: " + user + " " + arch + ">" << endl;

        } else if(msjjj.substr(0, 7) == "$sendme") {

            string aux = msjjj.substr(8);
            stringstream ss;
            ss << aux;
            string user, arch;
            getline(ss, user, '|');
            getline(ss, arch);
            string msjFile = "&file " + arch;
            string auxPort = to_string(puerto);

            int pid = fork();
            if (pid == 0) {

                // Si quien pide el archivo es el emisor del broadcast, no se realiza la transferencia.
                if (strcmp(inet_ntoa(client.sin_addr), ipLocal.c_str()) != 0) {
                    envioMensaje(inet_ntoa(client.sin_addr), (char *) auxPort.c_str(), msjFile, user, "");
                }
            }

        } else {

            stringstream ss;
            ss << buf;
            string user;
            string menso;
            getline(ss, user, '|');
            getline(ss, menso);

            string mens= clock + " "+ inet_ntoa(client.sin_addr) + " " + user + " dice: " + menso;    /* Mostrará la IP del remitente, y el mensaje luego de dice:  */
            loggear(mens);

            cout<<mens<<endl;
        }

        close(fd2);
    }
}

void recepcionMensajeBroadcast(int puerto) {
    int fd, err;
    struct sockaddr_in server{}, client{};

    fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (fd == -1) {
        cout << "[recepcionMensajeBroadcast] Error al crear el socket" << endl;

    }

    server.sin_family = AF_INET;
    server.sin_port = htons((uint16_t) puerto);
    server.sin_addr.s_addr = INADDR_ANY;

    bzero(&(server.sin_zero), 8);  // Escribimos ceros en el resto de la estructura

    err = bind(fd, (struct sockaddr *) &server, sizeof(server));
    if (err == -1) {
        cout << "[recepcionMensajeBroadcast] Error en bind()" << endl;

    }

    fileDescriptors.push_back(&fd);

    /*****************************/

    char buf[MAX_LARGO_MENSAJE];
    socklen_t l = sizeof(client);

    bool loop = true;
    while (loop) {

        ssize_t rc = recvfrom(fd, buf, (size_t) MAX_LARGO_MENSAJE, 0, (struct sockaddr *) &client, &l);
        if (rc < 0) {
            cout << "[recepcionMensajeBroadcast] Error en recvfrom()" << endl;
        }
        buf[rc-2] = '\0';

        string msjjj = (string) buf;
        if (msjjj.length() > 6 && msjjj.substr(0, 5) == "&file") {

            string aux = msjjj.substr(6);
            stringstream ss;
            ss << aux;
            string user, arch;
            getline(ss, user, '|');
            getline(ss, arch);

            string auxPort = to_string(puerto);
            envioMensaje(inet_ntoa(client.sin_addr), (char *) auxPort.c_str(), "$sendme " + arch, user, "");

        } else {

            string clock = reloj();
            stringstream ss;
            ss<<buf;
            string user;
            string menso;
            getline(ss, user, '|');
            getline(ss, menso);

            string mens= clock + " "+ inet_ntoa(client.sin_addr) + " " + user + " dice: " + menso;    /* Mostrará la IP del remitente, y el mensaje luego de dice:  */
            loggear(mens);
            cout<<mens<<endl;
        }
    }

    close(fd);
}

int receive_image(int socket, string nomArch) { // Start function

    int recv_size = 0, size = 0, read_size, write_size,
            packet_index = 1, stat;

    char imagearray[10241];
    FILE *image;

    // Find the size of the image
    do {
        stat = (int) read(socket, &size, sizeof(int));
    } while (stat < 0);

    char buffer[] = "Got it";

    // Send our verification signal
    do {
        stat = (int) write(socket, &buffer, sizeof(int));
    } while (stat < 0);

    image = fopen(nomArch.c_str(), "w");
    //TODO: verificar que el archivo no exista o usar w
    /* string clock = reloj();
    string mens= clock + " " + user + " x" + menso;    // Mostrará la IP del remitente, y el mensaje luego de dice:
    loggear(mens);
    */

    if (image == nullptr) {
        printf("Error has occurred. Image file could not be opened\n");
        return -1;
    }

    // Loop while we have not received the entire file yet
    struct timeval timeout = {10, 0};

    fd_set fds;
    int buffer_fd;

    while (recv_size < size) {

        FD_ZERO(&fds);
        FD_SET(socket, &fds);

        buffer_fd = select(FD_SETSIZE, &fds, nullptr, nullptr, &timeout);

        if (buffer_fd < 0)
            printf("error: bad file descriptor set.\n");

        if (buffer_fd == 0)
            printf("error: buffer read timeout expired.\n");

        if (buffer_fd > 0) {
            do {
                read_size = (int) read(socket, imagearray, 10241);
            } while (read_size < 0);


            // Write the currently read data into our image file
            write_size = (int) fwrite(imagearray, 1, (size_t) read_size, image);

            if (read_size != write_size) {
                printf("error in read write\n");
            }

            // Increment the total number of bytes read
            recv_size += read_size;
            packet_index++;
        }
    }

    fclose(image);

    return 1;
}
