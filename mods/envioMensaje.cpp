#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string>
#include <cstdio>
#include <netdb.h>
#include "constantes.h"
#include "envioMensaje.h"
#include <cstring>
#include "Logger.h"
#include "../mods_externos/md5.h"
#include "reloj.h"
#include <iostream>
#include <arpa/inet.h>
#include<fstream>
#include<cerrno>
#include <stdexcept>
#include <sstream>

using namespace std;

int envioMensaje(string ip, char *puerto, string mensaje, string autorizado, string ipLocal)
{
    int fd;    /* ficheros descriptores */
    struct hostent *he;   /* estructura que recibirá información sobre el nodo remoto */
    struct sockaddr_in client{};    /* información sobre la dirección del servidor */

    if (ip.empty()) {
        // 1 = error de direccionamiento IP
        return 1;
    }

    if ((he = gethostbyname(ip.c_str())) == nullptr){      /* llamada a gethostbyname() */
        throw runtime_error("[envioMensaje] Error en gethostbyname()");
    }

    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1){      /* llamada a socket() */
        //2 = error de socket
        return 2;
    }

    client.sin_family = AF_INET;
    client.sin_port = htons((uint16_t) stoi(puerto));     /* htons() es necesaria nuevamente ;-o */
    client.sin_addr = *((struct in_addr *) he->h_addr);    /*he->h_addr pasa la información de ``*he'' a "h_addr" */
    bzero(&(client.sin_zero),8);


    if (connect(fd, (struct sockaddr *) &client, sizeof(struct sockaddr)) == -1) {
        //Error en connect
        return 3;
    }


    if (mensaje.substr(0, 5) == "&file")
    {
        string archivoe = mensaje.substr(6);

        string nombre;
        size_t pos = archivoe.find_last_of('/');
        if (pos == string::npos) {
            // No hay ningua barra
            nombre = archivoe;
        } else {
            nombre = archivoe.substr(archivoe.find_last_of('/') + 1);
        }

        // Se envia paquete indicando que se enviara un archivo.
        string mensajeInicial = "&file " + autorizado + '|' + nombre + "\r\n";
        send(fd, mensajeInicial.c_str(), mensajeInicial.size(), 0);

        loggear(ip + " &file " + archivoe);

        // Se envia el archivo
        send_image(fd, archivoe);

        close(fd);

    } else if (mensaje.substr(0, 7) == "$sendme") {

        string envio = "$sendme " + autorizado + "|" + mensaje.substr(8) + "\r\n";
        send(fd, envio.c_str(), envio.size(), 0);
        close(fd);

    } else {
        string logmensaje=mensaje;
        mensaje += "\r\n";
        string msj = autorizado + "|" + mensaje;

        loggear(ip + ' ' + logmensaje);

        send(fd, msj.c_str(), msj.size(), 0);
        close(fd);
    }

    close(fd);
    return 0;
}



//////////////////////BROADCAST//////////////////////////

int envioMensajeBroadcast(char *puerto, string mensaje, string usuario, string ipLocal)
{

    int fd;    /* ficheros descriptores */
    struct sockaddr_in client{};    /* información sobre la dirección del servidor */
    string ip = "10.128.15.255";
    if (ip.empty()) {
        // 1 = error de direccionamiento IP
        return 1;
    }

    if ((fd = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP)) == -1){      /* llamada a socket() */
        // 2 = error de socket
        return 2;
    }

    int opt = 1;
    int err = setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (const char *) &opt, (socklen_t) sizeof(opt));
    if (err == -1) {
        throw runtime_error("[envioMensajeBroadcast] Error en setsockopt()");
    }

    client.sin_family = AF_INET;
    client.sin_port = htons((uint16_t) stoi(puerto));     /* htons() es necesaria nuevamente ;-o */
    client.sin_addr.s_addr = INADDR_BROADCAST;    /*he->h_addr pasa la información de ``*he'' a "h_addr" */
    bzero(&(client.sin_zero),8);

    string msj;

    if (mensaje.length() > 6 && mensaje.substr(0, 5) == "&file") {

        string archivoe = mensaje.substr(6);
        msj = "&file " + usuario + '|' + archivoe;

    } else {

        msj = usuario + "|" + mensaje;
    }

    msj += "\r\n";

    loggear( "* " + mensaje);

    ssize_t res = sendto(fd, msj.c_str(), msj.size(), 0, (struct sockaddr *) &client, sizeof(struct sockaddr));
    if (res == -1) {
        throw runtime_error("[envioMensajeBroadcast] Error en sendto()");
    }

    close(fd);
    return 0;
}





//////////////////////////////////////////ENVIO ARCHIVO///////////////////////////////////////////////////

int send_image(int socket, string arch) {

    FILE *picture;
    long size;
    int read_size, stat, packet_index;
    char send_buffer[10240], read_buffer[256];
    packet_index = 1;

    picture = fopen(arch.c_str(), "r");

    if (picture == nullptr) {
        printf("Error Opening Image File");
    }

    fseek(picture, 0, SEEK_END);
    size = ftell(picture);
    fseek(picture, 0, SEEK_SET);

    // Send Picture Size
    write(socket, (void *)&size, sizeof(int));

    // Send Picture as Byte Array

    do { // Read while we get errors that are due to signals.
        stat = (int) read(socket, &read_buffer, 255);
    } while (stat < 0);

    while (!feof(picture)) {
        // while(packet_index = 1){
        // Read from the file into our send buffer
        read_size = (int) fread(send_buffer, 1, sizeof(send_buffer) - 1, picture);

        // Send data through our socket
        do {
            stat = (int) write(socket, send_buffer, (size_t) read_size);
        } while (stat < 0);

        packet_index++;

        // Zero out our send buffer
        bzero(send_buffer, sizeof(send_buffer));
    }

    return 0;
}