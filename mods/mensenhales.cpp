
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include "constantes.h"
#include "mensenhales.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "reloj.h"
#include <netdb.h>
#include <sstream>
#include <list>
#include <stdexcept>

#define MAXDATASIZE 100 /* El número máximo de datos en bytes */


using namespace std;

void manejadorSenhales (int signal)



// Manejador de las senhales.
// Aca se debe implementar la accion a realizar cuando se recibe la senhal
// Deberia haber un manejador de senhales para cada hijo si hacen cosas distintas
{
    extern list<int *> fileDescriptors;

    if (signal == SIGINT)
    {
        cout << "\33[46m\33[31m[" << getpid() << "]" << " SIGINT CTRL+C recibido\33[00m\n";
    }
    if (signal == SIGTERM)
    {
        cout << "\33[46m\33[31m[" << getpid() << "]" << " SIGTERM Terminacion de programa\33[00m\n";
    }
    if (signal == SIGSEGV)
    {
        cout << "\33[46m\33[31m[" << getpid() << "]" << " SIGSEGV violacion de segmento\33[00m\n";
    }
    if (signal == SIGCHLD)
    {
        cout << "\33[46m\33[31m[" << "]" << " SIGCHLD \33[00m\n";
    }
    if (signal == SIGPIPE)
    {
        cout << "\33[46m\33[31m[" << getpid() << "]" << " SIGPIPE \33[00m\n";
    }
    if (signal == SIGKILL)
    {
        cout << "\33[46m\33[31m[" << getpid() << "]" << " SIGKILL \33[00m\n";
    }
    if (signal == SIGSEGV)
    {
        cout << "\33[46m\33[31m[" << getpid() << "]" << " SIGSEGV violacion de segmento\33[00m\n";
    }

    // Cerramos todos los fd que haya en la lista.
    for (auto *it : fileDescriptors) {
        close(*it);
    }

    exit(1);
}

bool cliente(char *ip, char *puerto, string usuario, string pass)
{
    int fd;    /* ficheros descriptores */
    ssize_t numbytes;
    char buf[MAXDATASIZE];  /* en donde es almacenará el texto recibido */
    struct hostent *he;   /* estructura que recibirá información sobre el nodo remoto */
    struct sockaddr_in client{};    /* información sobre la dirección del servidor */

    if (strcmp(ip, "\0") == 0) {
        throw runtime_error("[cliente] No se ha indicado una IP");
    }

    if ((he = gethostbyname(ip)) == nullptr){      /* llamada a gethostbyname() */
        throw runtime_error("[cliente] Error en gethostbyname()");
    }

    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1){      /* llamada a socket() */
        throw runtime_error("[client] Error en socket()");
    }

    client.sin_family = AF_INET;
    client.sin_port = htons((uint16_t) stoi(puerto));     /* htons() es necesaria nuevamente ;-o */
    client.sin_addr = *((struct in_addr *) he->h_addr);    /*he->h_addr pasa la información de ``*he'' a "h_addr" */
    bzero(&(client.sin_zero),8);


    if (connect(fd, (struct sockaddr *) &client, sizeof(struct sockaddr)) == -1) {
        cout << "Connect error: levantaste el server de autenticación?" << endl;
        cin.get();
        return false;
    }

    numbytes = recv(fd, buf, MAXDATASIZE, 0);
    if (numbytes == -1) {
        throw runtime_error("[cliente] Error en recv()");
    }
    buf[numbytes - 2]='\0';
    cout << "\nMensaje del Servidor: " << buf << endl;    /* muestra el mensaje de bienvenida del servidor */
    sleep(2);
    string uu = "" + usuario + "-" + pass + "\r\n";
    send(fd, uu.c_str(), uu.size(), 0);

    numbytes = recv(fd, buf, MAXDATASIZE, 0);
    if (numbytes == -1) {
        throw runtime_error("[cliente] Error en recv()");
    }
    buf[numbytes - 2]='\0';


    if (strcmp(buf, "SI") == 0) {
        cout << "Autenticado correctamente!" << endl;
        numbytes = recv(fd, buf, MAXDATASIZE, 0);
        if (numbytes == -1) {
            throw runtime_error("[cliente] Error en recv()");
        }

        buf[numbytes - 2]='\0';

        cout << "Nombre del usuario: " << buf << endl;

        close(fd);

        return true;

    } else {
        cout << "Autenticación fallida!" << endl;

        close(fd);

        return false;
    }
}
