#include <sys/types.h>
#include <sys/socket.h>
#include <termios.h>
#include <cstdio>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cstdlib>     /* system, NULL, EXIT_FAILURE */
#include <sstream>
#include <fstream>
#include "constantes.h"
#include "autenticar.h"
#include "../mods_externos/md5.h"
#include "mensenhales.h"
#include "Logger.h"
#include "reloj.h"

#define clearscr() printf("\033[H\033[J");

using namespace std;
static struct termios tioOld, tioNew;


// Initialize new terminal i/o settings
void hideTerminalInput()
{
	tcgetattr(STDIN_FILENO, &tioOld);           // grab old terminal i/o settings
	tioNew = tioOld;                            // make new settings same as old settings
	tioNew.c_lflag &= ~ECHO;                    // set no echo mode
	tcsetattr(STDIN_FILENO, TCSANOW, &tioNew);  // use these new terminal i/o settings now
}

// Restore old terminal i/o settings
void resetTerminalInput() 
{
	tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
}

// using std::cout; using std::endl;

string codificamd5(string &contra)
{
   return (md5(contra));
}



string doLogin(char *ipsrv, char *puertosrv)
{
    bool autenticado;
    int i=0;
    //unsigned int puerto;
    string usuario;
    string contra;
    string pass;

    //Se solicita USURIO
    //clearscr();
    cout << "              MENSAJERIA               " <<endl;
    cout << "             Inicie Sesión           \n" <<endl;
    cout << "Ingrese su Usuario: ";
    getline(cin, usuario);

    do
    {
    // Se oculta lo escrito en terminal
    hideTerminalInput();

    // Se solicita CONTRASEÑA
    cout << "Ingrese su contraseña: ";
    // Se lee de la consola sin ver lo que se escribe
    getline(cin, contra);

    // Se reinicia la terminal a su comportamiento habitual
    resetTerminalInput();

    // Codificar por medio de función auxiliar
    pass = (codificamd5(contra));

    // Llamar a función para logueo  en el servicio de autenticación
    autenticado=cliente(ipsrv, puertosrv, usuario, pass);

    // Verificar resultado CONDICIÓN si OK, vuelve al main con OK si no, re intetar log in USR/PASS

        i++;
        cout << endl;
    }
    while(!autenticado && i<AUTENTICAR);
    if(autenticado)
    {   string logAutenticado = reloj() + "->Autenticado->" + usuario + " (" + ipsrv + ":" + puertosrv + ")";
        loggear(logAutenticado);
        return usuario;
    }

    return "NOAUTORIZADO";
}