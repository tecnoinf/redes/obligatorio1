#ifndef OBLIGATORIO1_ENVIOMENSAJE_H
#define OBLIGATORIO1_ENVIOMENSAJE_H

using namespace std;

int envioMensaje(string ip, char *puerto, string mensaje, string autorizado, string ipLocal);
int envioMensajeBroadcast(char *puerto, string mensaje, string autorizado, string ipLocal);
int send_image(int socket, string arch);

#endif //OBLIGATORIO1_ENVIOMENSAJE_H
