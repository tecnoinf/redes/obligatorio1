#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <string>
using namespace std;

unsigned int micros;

string reloj()
{
	micros=1000000;
	usleep(micros);
	time_t t = time(nullptr);

	struct tm tm = *localtime(&t);
	string sd = "[" + to_string(tm.tm_year + 1900) + "." + to_string(tm.tm_mon + 1) + "." + to_string(tm.tm_mday) + " " + to_string(tm.tm_hour) + ":" + to_string(tm.tm_min) + ":" + to_string(tm.tm_sec) + "]";

    return sd;
}

