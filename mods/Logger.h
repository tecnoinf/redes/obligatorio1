//
// Created by sebiitta on 04/06/18.
//

#ifndef OBLIGATORIO1_LOGGER_H
#define OBLIGATORIO1_LOGGER_H

#define LOGGER_PORT 30003  // Puerto para el servidor de log
#define CONNS_LIMIT 2      // Limite de conexiones simultaneas

#include <string>

using std::string;

void logger();
bool loggear(string mensaje);

#endif //OBLIGATORIO1_LOGGER_H
