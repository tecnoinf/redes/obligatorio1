#ifndef OBLIGATORIO1_RECEPCIONMENSAJE_H
#define OBLIGATORIO1_RECEPCIONMENSAJE_H
using namespace std;

void recepcionMensaje(int puerto, string ipLocal);
void recepcionMensajeBroadcast(int puerto);
int receive_image(int socket, string nomArch);

#endif //OBLIGATORIO1_RECEPCIONMENSAJE_H
