/*
 * Redes de Computadoras - Curso 1er Semestre 2018
 * Tecnologo en Informatica FING - CETP
 *
 * Obligatorio  - Programacion con Sockets
 * Sistema basico de Mensajeria
 *
 * Integrantes:
 *      Sebastián Rodríguez, CI: 5.466.754-1
 *      Jorge Leguizamón,    CI: 4.681.485-9
 *      Sebastián Ortiz,     CI: 4.439.855-6
*/

#include <ifaddrs.h>
#include <arpa/inet.h>


#include <sys/wait.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include "mods/Logger.h"
#include <list>
#include <map>
#include "mods/constantes.h"
#include "mods/autenticar.h"
#include "mods/mensenhales.h"
#include "mods/envioMensaje.h"
#include "mods/reloj.h"
#include "mods/recepcionMensaje.h"

#define clearscr() printf("\033[H\033[J");

using namespace std;

list<int *> fileDescriptors;


string getIPAddress()
{
    string ipAddress="Problemas para mostrar la dirección IP";
    struct ifaddrs *interfaces = nullptr;
    struct ifaddrs *temp_addr = nullptr;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != nullptr) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if(strcmp(temp_addr->ifa_name, "en0")){
                    ipAddress=inet_ntoa(((struct sockaddr_in*)temp_addr->ifa_addr)->sin_addr);
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return ipAddress;

}





int main(int argc, char * argv[])
// En argc viene la cantidad de argumentos que se pasan,
// si se llama solo al programa, el nombre es el argumento 0
// En nuestro caso:
//      - argv[0] es el string "mensajeria", puede cambiar si se llama con otro path.
//      - argv[1] es el puerto de escucha de mensajeria.
//      - argv[2] es el ip del servidor de autenticacion.
//      - argv[3] es el puerto del servidor de autenticacion.
{
    string ipLocal= getIPAddress();

    // Comprobamos que se hayan ingresado todos los parametros necesarios.
    if (argc < 4) {
        cout << "\33[46m\33[31m[ERROR]:" << " Faltan argumentos: port, ipAuth, portAuth.\33[00m\n";
        exit(1);
    }

    clearscr();


    // Estructuras para el manejo de Senhales
    // Deberia haber un manejador de senhales para cada hijo si hacen cosas distintas
    // *********************************
    struct sigaction sa{};
    memset (&sa, 0, sizeof (sa));
    sa.sa_handler = &manejadorSenhales;

    sigaction(SIGINT, &sa, nullptr);
    sigaction(SIGTERM, &sa, nullptr);
    sigaction(SIGPIPE, &sa, nullptr);
    sigaction(SIGSEGV, &sa, nullptr);
    sigaction(SIGKILL, &sa, nullptr);
    signal(SIGALRM, SIG_IGN);
    // **********************************


    int pid1 = fork();

    if (pid1 < 0) {
        cout << "\33[46m\33[31m[ERROR]:" << " Imposible Bifurcar.\33[00m\n";
        exit(1);

    } else if (pid1 == 0) {
        // Proceso hijo.
        // Se inicia el proceso de log.
        // Se crea un socket TCP que escucha en el puerto definido en la constante 'LOGGER_PORT'
        // dentro de 'Logger.h'.
        logger();

    } else {
        // 'autorizado' guardara el usaurio autenticado, si la autenticacion es exitosa.
        string autorizado;

        loggear("<<<" + reloj() + " - pid=" + to_string(getpid()) + " - "+ ipLocal + ":" + argv[1] + ">>>");

        // Se autentica contra el servidor de autenticacion.
        do {
            autorizado = doLogin(argv[2], argv[3]);
        }
        while (autorizado == "NOAUTORIZADO");

        clearscr();
        cout << "\n\33[34mRedes de Computadoras 2018\33[39m: Sistema de Mensajeria.\nEscuchando en el puerto " << argv[1]
             << ".\nPID: " << getpid() << ".\n";


        // Se bifurca el programa para enviar y recibir mensajes en procesos separados.
        int pid2 = fork();

        if (pid2 < 0) {
            cout << "\33[46m\33[31m[ERROR]:" << " Imposible Bifurcar.\33[00m\n";
            exit(1);

        } else if (pid2 == 0) {
            // Proceso hijo.
            cout << "\33[34mRx\33[39m: Iniciada parte que recepciona mensajes. Pid " << getpid() << endl;


            // Se bifurca el programa para recibir los mensajes por TCP y UDP en procesos separados.
            int pid3 = fork();

            if (pid3 < 0) {
                cout << "\33[46m\33[31m[ERROR]:" << " Imposible Bifurcar.\33[00m\n";
                exit(1);

            } else if (pid3 == 0) {
                // Iniciamos proceso de recepcion de mensajes por TCP.
                recepcionMensaje(stoi(argv[1]), ipLocal);

            } else {
                // Iniciamos proceso de recepcion de mensajes por UDP.
                recepcionMensajeBroadcast(stoi(argv[1]));
            }

        } else {
            // Este es el proceso padre. El padre recibe el PID del hijo, para poder matarlo.
            cout << "\33[34mTx\33[39m: Iniciada parte que transmite mensajes. Pid " << getpid() << endl;

            while (true) {

                string mensaje, dir;
                getline(cin, dir, ' ');
                getline(cin, mensaje);


                if(dir != "*") {

                    if ((envioMensaje(dir, argv[1], mensaje, autorizado, ipLocal)) == 0) {}

                } else {

                     if ((envioMensajeBroadcast(argv[1], mensaje, autorizado, ipLocal)) == 0) {}
                }
            }
        }
    }

    return 0;
    // TODO: Limpiar pantalla al iniciar programa
    // TODO: Error con lineas vacias
    // TODO: Al morir el padre, deben morir todos los hijos...
}



// Es indistinto si el padre transmite y el hijo recibe, o viceversa, lo que si al ser distintos porcesos, van a tener distinto pid.
// Si se crean nuevos procesos para distintas funcionalidades, agregar un mensaje como el de arriba al principio de su ejecucion
// describiendo su funcionalidad.
// Familiarizarse con los comandos de UNIX ps, ps -as, kill, etc.
